# Dummy Odoo package

You can use this package to fool odoo addons that depend on Odoo.

We may want to use it when you installed Odoo in other ways than using the odoo-installer pypi package.

## Usage

Add this line to your `requirements.txt` and execute `pip install -r requirements.txt`:
```
-e git+https://gitlab.com/coopdevs/dummy-odoo-pypi-package@12.0#egg=odoo
```

Alternatively, you can run `pip install` followed by the line above.
